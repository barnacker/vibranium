#include "host_driver.h"
#include "quantum_keycodes.h"

const uint16_t PROGMEM hd_tab[]      = {LCTL_T(KC_S), LALT_T(KC_C), COMBO_END};
const uint16_t PROGMEM hd_esc[]      = {LCTL_T(KC_S), LSFT_T(KC_T), COMBO_END};
const uint16_t PROGMEM hd_capsword[] = {LSFT_T(KC_T), RSFT_T(KC_A), COMBO_END};
const uint16_t PROGMEM qw_tab[]      = {LCTL_T(KC_A), LALT_T(KC_S), COMBO_END};
const uint16_t PROGMEM qw_esc[]      = {LCTL_T(KC_A), LSFT_T(KC_F), COMBO_END};
const uint16_t PROGMEM qw_capsword[] = {LSFT_T(KC_F), RSFT_T(KC_J), COMBO_END};
const uint16_t PROGMEM I3W_combo[]   = {TD(LEFT_DANCE), TD(RIGHT_DANCE), COMBO_END};
const uint16_t PROGMEM H_Th_combo[]  = {LSFT_T(KC_T), LGUI_T(KC_N), COMBO_END}; // TYPE "th"
const uint16_t PROGMEM H_Ch_combo[]  = {RALT_T(KC_C), LGUI_T(KC_N), COMBO_END}; // TYPE "ch"
const uint16_t PROGMEM H_Wh_combo[]  = {KC_W, KC_M, COMBO_END};                 // TYPE "wh"
// const uint16_t PROGMEM H_Sh_combo[]  = {RCTL_T(KC_S), RALT_T(KC_C), COMBO_END};        COMBO(H_Sh_combo, H_Sh),       // TYPE "sh"
const uint16_t PROGMEM H_Ph_combo[]  = {KC_P, KC_L, COMBO_END};                               // TYPE "ph"
const uint16_t PROGMEM H_Gh_combo[]  = {KC_M, KC_G, COMBO_END};                               // TYPE "gh"
const uint16_t PROGMEM H_Sch_combo[] = {RCTL_T(KC_S), RALT_T(KC_C), LGUI_T(KC_N), COMBO_END}; // TYPE "Sch"

combo_t key_combos[] = {COMBO(I3W_combo, MO(_I3W)), COMBO(hd_tab, KC_TAB), COMBO(qw_tab, KC_TAB), COMBO(hd_esc, KC_ESC), COMBO(hd_capsword, CW_TOGG), COMBO(qw_esc, KC_ESC), COMBO(qw_capsword, CW_TOGG), COMBO(H_Wh_combo, H_W), COMBO(H_Th_combo, H_T), COMBO(H_Ch_combo, H_C), COMBO(H_Ph_combo, H_P), COMBO(H_Gh_combo, H_G), COMBO(H_Sch_combo, H_Sch)};
