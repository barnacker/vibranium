#include <stdint.h>
#include "quantum_keycodes.h"

#include "action.h"
#include "action_util.h"
#include "progmem.h"

#include "adaptive_keys_vb_def.h"

static uint32_t adaptive_timer = 0;
static uint16_t prior_seq[2]   = {KC_NO, KC_NO};

void tap_code_sequence(int from, uint16_t *output_sequence) {
    for (int i = from; i < 3 && output_sequence[i] != KC_NO; i++) {
        tap_code(output_sequence[i]);
    }
}

// Optimized to replace only the different characters
void tap_adaptive_seq(uint16_t *output_sequence) {
    if (prior_seq[0] != output_sequence[0]) {
        if (prior_seq[1] != KC_NO) {
            tap_code(KC_BSPC);
        }
        tap_code(KC_BSPC);
        tap_code_sequence(0, output_sequence);
    } else if (prior_seq[1] != output_sequence[0]) {
        if (prior_seq[1] != KC_NO) {
            tap_code(KC_BSPC);
        }
        tap_code_sequence(1, output_sequence);
    } else {
        tap_code_sequence(2, output_sequence);
    }
}

void reset_adaptive_keys(void) {
    adaptive_timer = 0;
    prior_seq[0]   = KC_NO;
    prior_seq[1]   = KC_NO;
}

int find_adaptive_pattern(uint16_t keycode) {
    int pattern_count = sizeof(adaptive_patterns) / sizeof(adaptive_patterns[0]);
    for (int i = 0; i < pattern_count; i++) {
        uint16_t input_pattern[3];
        for (int m = 0; m < 3; m++) {
            input_pattern[m] = pgm_read_byte(&adaptive_patterns[i][0][m]);
        };

        uint16_t output_sequence[3];
        for (int m = 0; m < 3; m++) {
            output_sequence[m] = pgm_read_byte(&adaptive_patterns[i][1][m]);
        };

        if (adaptive_timer == 0 && keycode == input_pattern[0]) {
            prior_seq[0]   = keycode;
            adaptive_timer = timer_read32();
            return 1;
        }

        if (timer_elapsed32(adaptive_timer) <= ADAPTIVE_TERM && prior_seq[0] != KC_NO && prior_seq[1] == KC_NO && keycode == input_pattern[1] && prior_seq[0] == input_pattern[0]) {
            if (input_pattern[2] == KC_NO) {
                if (!custom_actions(output_sequence[0])) { // if not a custom key, send the key
                    tap_adaptive_seq(output_sequence);
                }

                reset_adaptive_keys();
                return 3;
            }
            prior_seq[1]   = keycode;
            adaptive_timer = timer_read32();
            return 2;
        }

        if (timer_elapsed32(adaptive_timer) <= ADAPTIVE_TERM && prior_seq[0] != KC_NO && prior_seq[1] != KC_NO && keycode == input_pattern[2] && prior_seq[0] == input_pattern[0] && prior_seq[1] == input_pattern[1]) {
            tap_adaptive_seq(output_sequence);
            reset_adaptive_keys();
            return 4;
        }
    }
    return 0;
}

// This function is called from process_record_user
bool process_adaptive_keys(uint16_t keycode, keyrecord_t *record) {
    if (record->event.pressed) {
        uint8_t saved_mods = get_mods();
        if (saved_mods & MOD_MASK_CAG) {
            // don't interfere if modifiers are involved
            return false;
        }

        keycode = keycode & 0xFF;

        int pattern_found = find_adaptive_pattern(keycode);
        if (pattern_found == 0 && prior_seq[0] != KC_NO) {
            prior_seq[0]   = keycode;
            prior_seq[1]   = KC_NO;
            adaptive_timer = 0;
            pattern_found  = find_adaptive_pattern(keycode);
        }
        switch (pattern_found) {
            case 1 ... 2:
                return false;
            case 3 ... 4:
                return true;
        }
    }
    return false;
}
