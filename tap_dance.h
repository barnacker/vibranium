#include QMK_KEYBOARD_H
// Define a type for as many tap dance states as you need
typedef enum {
  TD_NONE,
  TD_UNKNOWN,
  TD_SINGLE_TAP,
  TD_SINGLE_HOLD,
  TD_DOUBLE_TAP,
  TD_DOUBLE_TAP_HOLD,
	TD_TRIPLE_TAP
} td_state_t;

typedef struct {
  bool is_press_action;
  td_state_t state;
} td_tap_t;

enum {
  LEFT_DANCE, // Our custom tap dance key; add any other tap dance keys to this enum
  RIGHT_DANCE,
};

// Declare the functions to be used with your tap dance key(s)

// Function associated with all tap dances
td_state_t cur_dance(tap_dance_state_t *state);

// Functions associated with individual tap dances
void left_done(tap_dance_state_t *state, void *user_data);
void left_reset(tap_dance_state_t *state, void *user_data);

void right_done(tap_dance_state_t *state, void *user_data);
void right_reset(tap_dance_state_t *state, void *user_data);

// Determine the current tap dance state
td_state_t cur_dance(tap_dance_state_t *state) {
  if (state->count == 1) {
    if (!state->pressed) {
      return TD_SINGLE_TAP;
		}
    else {
      return TD_SINGLE_HOLD;
		}
  } else if (state->count == 2) {
		if (!state->pressed) {
    	return TD_DOUBLE_TAP;
		}
    else {
    	return TD_DOUBLE_TAP_HOLD;
		}
	} else if (state->count == 3) {
    	return TD_TRIPLE_TAP;
	}

  return TD_UNKNOWN;
}

// Initialize tap structure associated with example tap dance key
static td_tap_t left_tap_state = {.is_press_action = true, .state = TD_NONE};

// Functions that control what our tap dance key does
void left_done(tap_dance_state_t *state, void *user_data) {
  left_tap_state.state = cur_dance(state);
  switch (left_tap_state.state) {
  case TD_SINGLE_HOLD:
    layer_on(_FNK);
    break;
  case TD_DOUBLE_TAP:
    break;
  case TD_DOUBLE_TAP_HOLD:
    layer_on(_I3W);
    break;
	case TD_TRIPLE_TAP:
    reset_keyboard();
    break;
  default:
    break;
  }
}

void left_reset(tap_dance_state_t *state, void *user_data) {
  // If the key was held down and now is released then switch off the layer
  if (left_tap_state.state == TD_SINGLE_HOLD) {
    layer_off(_FNK);
  } else if (left_tap_state.state == TD_DOUBLE_TAP_HOLD) {
    layer_off(_I3W);
	}
  left_tap_state.state = TD_NONE;
}

// Initialize tap structure associated with example tap dance key
static td_tap_t right_tap_state = {.is_press_action = true, .state = TD_NONE};

// Functions that control what our tap dance key does
void right_done(tap_dance_state_t *state, void *user_data) {
  right_tap_state.state = cur_dance(state);
  switch (right_tap_state.state) {
  case TD_SINGLE_HOLD:
    layer_on(_OPT);
    break;
  case TD_DOUBLE_TAP:
    // Check to see if the layer is already set
    if (default_layer_state == 0x02) {
      // If already set, then switch it off
      set_single_persistent_default_layer(_BASE);
    } else {
      // If not already set, then switch the layer on
      set_single_persistent_default_layer(_QWERTY);
    }
    break;
  case TD_DOUBLE_TAP_HOLD:
    layer_on(_F12);
    break;
	case TD_TRIPLE_TAP:
    layer_on(_LRGB);
    break;
  default:
    break;
  }
}

void right_reset(tap_dance_state_t *state, void *user_data) {
  // If the key was held down and now is released then switch off the layer
  if (right_tap_state.state == TD_SINGLE_HOLD) {
    layer_off(_OPT);
  } else if (right_tap_state.state == TD_DOUBLE_TAP_HOLD) {
		layer_off(_F12);
	}
  right_tap_state.state = TD_NONE;
}

// Associate our tap dance key with its functionality
tap_dance_action_t tap_dance_actions[] = {
    [LEFT_DANCE] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, left_done, left_reset),
    [RIGHT_DANCE] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, right_done, right_reset),
};

// Set a long-ish tapping term for tap-dance keys
uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
  case QK_TAP_DANCE ... QK_TAP_DANCE_MAX:
    return 275;
  default:
    return TAPPING_TERM;
  }
}
