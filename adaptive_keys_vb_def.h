#include "quantum_keycodes.h"
#include "action.h"
enum {
    AK_COM,
    AK_IO,
};

const uint16_t PROGMEM adaptive_patterns[][2][3] = {
    // Left hand
    {{KC_B, KC_C, KC_NO}, {KC_B, KC_S, KC_NO}}, // BS is 23x more common than BC
    {{KC_B, KC_P, KC_NO}, {KC_S, KC_P, KC_NO}}, // SP is 83x more common than BP
    {{KC_C, KC_B, KC_NO}, {KC_S, KC_B, KC_NO}}, // CB is 11x more common than SB
    {{KC_D, KC_H, KC_NO}, {KC_D, KC_QUOT, KC_H}}, // dh=d'h
    {{KC_D, KC_P, KC_NO}, {KC_D, KC_T, KC_NO}}, // DP = DT eliminate SFB (DT is 2.5x more common)
    {{KC_G, KC_M, KC_NO}, {KC_G, KC_L, KC_NO}}, // GL is 5x more common than GM
    {{KC_G, KC_W, KC_NO}, {KC_G, KC_D, KC_NO}}, // gw=gd
    {{KC_G, KC_X, KC_NO}, {KC_G, KC_T, KC_NO}}, // gx=gt
    {{KC_J, KC_G, KC_NO}, {KC_J, KC_P, KC_G}},  // jg=jpg
    {{KC_J, KC_H, KC_NO}, {KC_J, KC_QUOT, KC_H}}, // jh=j'h
    {{KC_K, KC_G, KC_NO}, {KC_K, KC_L, KC_NO}}, // kg=kl
    {{KC_K, KC_T, KC_NO}, {KC_K, KC_N, KC_NO}}, // "kt" yields "kn" (+48x)
    {{KC_L, KC_D, KC_L}, {KC_L, KC_M, KC_L}},   // ldl=lml
    {{KC_L, KC_H, KC_NO}, {KC_L, KC_QUOT, KC_H}}, // lh=l'h
    {{KC_M, KC_G, KC_NO}, {KC_L, KC_G, KC_NO}}, // mg=lg
    {{KC_M, KC_H, KC_NO}, {KC_M, KC_QUOT, KC_H}}, // mh=m'h
    {{KC_N, KC_H, KC_NO}, {KC_N, KC_QUOT, KC_H}}, // nh=n'h
    {{KC_M, KC_W, KC_M}, {KC_M, KC_P, KC_L}},   // "mwm" results in "mpl" trigram instead of scissor
    {{KC_M, KC_X, KC_NO}, {KC_M, KC_B, KC_NO}}, // "MB" is 2558x more frequent than "MX"
    {{KC_M, KC_X, KC_M}, {KC_M, KC_B, KC_L}},   // "mxm" results in "mbl" trigram instead of scissor
    {{KC_P, KC_B, KC_NO}, {KC_P, KC_S, KC_NO}}, // PS is 40x more common than PB
    {{KC_P, KC_M, KC_NO}, {KC_P, KC_L, KC_NO}}, // PL is 15x more common than PM
    {{KC_T, KC_C, KC_NO}, {KC_T, KC_C, KC_H}},  // 85% of tc is tch, so this saves a lot of key press "H"
    {{KC_T, KC_K, KC_NO}, {KC_L, KC_K, KC_NO}}, // TK/DK/GK = LK
    {{KC_X, KC_M, KC_NO}, {KC_X, KC_L, KC_NO}}, // XL is 1.5x more common than XM
    {{KC_X, KC_W, KC_M}, {KC_X, KC_P, KC_L}},   // "xwm" is also captured here, resulting in "xpl"

    // right hand

    {{KC_A, KC_H, KC_NO}, {KC_A, KC_U, KC_NO}},   // "AH" yields "AU"
    {{KC_E, KC_H, KC_NO}, {KC_E, KC_O, KC_NO}},   // "EH" yields "EO" (1.75:1)
    {{KC_I, KC_H, KC_NO}, {KC_I, KC_F, KC_NO}},   // "IH" yields "IF" (96x more common)
    {{KC_O, KC_H, KC_NO}, {KC_O, KC_E, KC_NO}},   // "OH" yields "OE" (almost 1:1, but eliminates an SFB?)
    {{KC_U, KC_H, KC_NO}, {KC_U, KC_A, KC_NO}},   // "UH" yields "UA" (126x more common)
    {{KC_Y, KC_H, KC_NO}, {KC_Y, KC_QUOT, KC_H}}, // yh=y'h
    {{KC_Y, KC_F, KC_NO}, {KC_Y, KC_I, KC_NO}},   // YF = YI (eliminate SFB on ring finger YI is 37x YF)
	//
		// useful macros for domain names
    {{KC_DOT, KC_QUOT, KC_NO}, {AK_COM, KC_NO, KC_NO}},
    {{KC_DOT, KC_SLSH, KC_NO}, {AK_IO, KC_NO, KC_NO}},

    // Double a key on a roll
    {{KC_A, KC_BSPC, KC_NO}, {KC_A, KC_A, KC_NO}},
    {{KC_B, KC_BSPC, KC_NO}, {KC_B, KC_B, KC_NO}},
    {{KC_C, KC_BSPC, KC_NO}, {KC_C, KC_C, KC_NO}},
    {{KC_D, KC_BSPC, KC_NO}, {KC_D, KC_D, KC_NO}},
    {{KC_E, KC_BSPC, KC_NO}, {KC_E, KC_E, KC_NO}},
    {{KC_F, KC_BSPC, KC_NO}, {KC_F, KC_F, KC_NO}},
    {{KC_G, KC_BSPC, KC_NO}, {KC_G, KC_G, KC_NO}},
    {{KC_H, KC_BSPC, KC_NO}, {KC_H, KC_H, KC_NO}},
    {{KC_I, KC_BSPC, KC_NO}, {KC_I, KC_I, KC_NO}},
    {{KC_J, KC_BSPC, KC_NO}, {KC_J, KC_J, KC_NO}},
    {{KC_K, KC_BSPC, KC_NO}, {KC_K, KC_K, KC_NO}},
    {{KC_L, KC_BSPC, KC_NO}, {KC_L, KC_L, KC_NO}},
    {{KC_M, KC_BSPC, KC_NO}, {KC_M, KC_M, KC_NO}},
    {{KC_N, KC_BSPC, KC_NO}, {KC_N, KC_N, KC_NO}},
    {{KC_O, KC_BSPC, KC_NO}, {KC_O, KC_O, KC_NO}},
    {{KC_P, KC_BSPC, KC_NO}, {KC_P, KC_P, KC_NO}},
    {{KC_Q, KC_BSPC, KC_NO}, {KC_Q, KC_Q, KC_NO}},
    {{KC_R, KC_BSPC, KC_NO}, {KC_R, KC_R, KC_NO}},
    {{KC_S, KC_BSPC, KC_NO}, {KC_S, KC_S, KC_NO}},
    {{KC_T, KC_BSPC, KC_NO}, {KC_T, KC_T, KC_NO}},
    {{KC_U, KC_BSPC, KC_NO}, {KC_U, KC_U, KC_NO}},
    {{KC_V, KC_BSPC, KC_NO}, {KC_V, KC_V, KC_NO}},
    {{KC_W, KC_BSPC, KC_NO}, {KC_W, KC_W, KC_W}},
    {{KC_X, KC_BSPC, KC_NO}, {KC_X, KC_X, KC_X}},
    {{KC_Y, KC_BSPC, KC_NO}, {KC_Y, KC_Y, KC_NO}},
    {{KC_Z, KC_BSPC, KC_NO}, {KC_Z, KC_Z, KC_NO}},
    {{KC_QUOT, KC_BSPC, KC_NO}, {KC_QUOT, KC_QUOT, KC_NO}},
    {{KC_COMM, KC_BSPC, KC_NO}, {KC_COMM, KC_COMM, KC_NO}},
    {{KC_DOT, KC_BSPC, KC_NO}, {KC_DOT, KC_DOT, KC_DOT}},
    {{KC_SLSH, KC_BSPC, KC_NO}, {KC_SLSH, KC_SLSH, KC_NO}},
    {{KC_BSLS, KC_DEL, KC_NO}, {KC_BSLS, KC_BSLS, KC_NO}},
    {{KC_GRV, KC_DEL, KC_NO}, {KC_GRV, KC_GRV, KC_GRV}},
    {{KC_MINS, KC_BSPC, KC_NO}, {KC_MINS, KC_MINS, KC_NO}},
    {{KC_SCLN, KC_DEL, KC_NO}, {KC_SCLN, KC_SCLN, KC_NO}},
};
bool custom_actions(uint16_t keycode) {
    switch (keycode) {
        case AK_COM:
            SEND_STRING("com");
            return true;
        case AK_IO:
            SEND_STRING("io");
            return true;
    }
    return false;
}
