# QMK software features

LEADER_ENABLE = no          # Enable the Leader Key feature
MOUSEKEY_ENABLE = no        # Mouse keys
UNICODE_ENABLE = no         # will need this for HD Polyglot
NKRO_ENABLE = no            # Enable N-Key Rollover
KEY_OVERRIDE_ENABLE = no
SPACE_CADET_ENABLE = no
GRAVE_ESC_ENABLE = no
MAGIC_ENABLE = no
BOOTMAGIC_ENABLE = no       # Enable Bootmagic Lite
CONSOLE_ENABLE = no
COMMAND_ENABLE = no
MOUSEKEY_ENABLE = no
EXTRAKEY_ENABLE = no
RGBLIGHT_ENABLE = no        # Enable keyboard RGB underglow
AUTO_SHIFT_ENABLE = no      # requires 1936 bytes!
TRI_LAYER_ENABLE = no       # disable tri layer
MUSIC_ENABLE = no
LTO_ENABLE = yes			# Link Time Optimization
EXTRAKEY_ENABLE = yes       # Audio control and System control
COMBO_ENABLE = yes          # chording
CAPS_WORD_ENABLE = yes	    # caps word
SEND_STRING_ENABLE = yes	# send string
AVR_USE_MINIMAL_PRINTF = yes
TAP_DANCE_ENABLE = yes
