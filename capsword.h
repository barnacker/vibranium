// for keyboard-level data sync:
typedef struct _master_to_slave_t {
    bool caps_on;
} master_to_slave_t;

typedef struct _slave_to_master_t {
    bool caps_on;
} slave_to_master_t;

void user_sync_capsword_slave_handler(uint8_t in_buflen, const void *in_data, uint8_t out_buflen, void *out_data) {
    const master_to_slave_t *m2s = (const master_to_slave_t *)in_data;
    slave_to_master_t *      s2m = (slave_to_master_t *)out_data;

    capsOn       = m2s->caps_on;
    s2m->caps_on = capsOn;
}

void keyboard_post_init_user(void) {
    transaction_register_rpc(CAPSWORD_SYNC, user_sync_capsword_slave_handler);
}

void caps_word_set_user(bool active) {
    if (active) {
        // Do something when Caps Word activates.
        capsOn = true;
    } else {
        // Do something when Caps Word deactivates.
        capsOn = false;
    }
    transaction_rpc_send(CAPSWORD_SYNC, sizeof(capsOn), &capsOn);
}

void housekeeping_task_user(void) {
    if (!capsOnSync && is_keyboard_master()) {
        // Interact with slave every 500ms
        static uint32_t last_sync = 0;
        if (timer_elapsed32(last_sync) > 500) {
            master_to_slave_t m2s = {capsOn};
            slave_to_master_t s2m = {};
            if (transaction_rpc_exec(CAPSWORD_SYNC, sizeof(m2s), &m2s, sizeof(s2m), &s2m)) {
                last_sync = timer_read32();
                if (capsOn == s2m.caps_on) {
                    capsOnSync = true;
                }
            }
        }
    }
}
