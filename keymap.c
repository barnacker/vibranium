// Copyright 2024 Barnacker
// SPDX-License-Identifier: GPL-2.0-or-later

#include QMK_KEYBOARD_H
#include "keymap.h"
#include "adaptive_keys.h"
#include "linger_keys.h"
#include "matrix.h"
#include "rgb.c"
#include "tap_dance.h"
#include "combo.c"

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    [_BASE] = LAYOUT(
        // ┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        XXXXXXX, KC_X, KC_W, KC_M, KC_G, KC_J, KC_HASH, KC_U, KC_O, KC_Y, KC_F, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        XXXXXXX, LCTL_T(KC_S), LALT_T(KC_C), LGUI_T(KC_N), LSFT_T(KC_T), KC_K, KC_DQUO, RSFT_T(KC_A), LGUI_T(KC_E), RALT_T(KC_I), RCTL_T(KC_H), XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
        XXXXXXX, KC_B, KC_P, KC_L, KC_D, KC_V, KC_R, KC_SPC, KC_MINS, KC_QUOT, KC_COMM, KC_DOT, KC_SLSH, XXXXXXX,
        // └────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
        TD(LEFT_DANCE), KC_R, KC_BSPC, KC_ENT, KC_SPC, TD(RIGHT_DANCE)
        // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
        ),

    [_QWERTY] = LAYOUT(
        // ┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        _______, LINGER_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        _______, LCTL_T(KC_A), LALT_T(KC_S), LGUI_T(KC_D), LSFT_T(KC_F), KC_G, KC_H, RSFT_T(KC_J), LGUI_T(KC_K), RALT_T(KC_L), RCTL_T(KC_SCLN), KC_QUOT,
        // ├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
        _______, KC_Z, KC_X, KC_C, KC_V, KC_B, _______, _______, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, XXXXXXX,
        // └────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
        _______, _______, _______, _______, _______, _______
        // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
        ),

    [_FNK] = LAYOUT(
        // ┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, XXXXXXX, KC_HOME, KC_UP, KC_END, LCTL(KC_F5), KC_PAST, KC_7, KC_8, KC_9, KC_PPLS, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, KC_LCTL, KC_LEFT, KC_DOWN, KC_RGHT, XXXXXXX, KC_PSLS, KC_4, KC_5, KC_6, KC_MINS, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, KC_LSFT, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_DOT, KC_1, KC_2, KC_3, KC_EQL, XXXXXXX,
        // └────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_0, MO(_F12)
        // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
        ),

    [_OPT] = LAYOUT(
        // ┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
        EE_CLR, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        XXXXXXX, LINGER_Q, KC_Z, XXXXXXX, XXXXXXX, XXXXXXX, KC_DLR, KC_TILD, KC_AT, KC_PERC, KC_HASH, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        XXXXXXX, KC_LCBR, KC_LPRN, KC_RPRN, KC_RCBR, XXXXXXX, KC_CIRC, KC_QUES, KC_EXLM, KC_AMPR, KC_PIPE, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
        XXXXXXX, KC_LBRC, KC_LT, KC_GT, KC_RBRC, XXXXXXX, XXXXXXX, XXXXXXX, KC_UNDS, KC_GRV, KC_SCLN, KC_COLN, KC_BSLS, XXXXXXX,
        // └────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
        TG(_GAM), XXXXXXX, KC_DEL, XXXXXXX, XXXXXXX, XXXXXXX
        // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
        ),

    [_GAM] = LAYOUT(
        // ┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
        KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_RGUI, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        KC_F2, KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_PAST, KC_7, KC_8, KC_9, KC_PPLS, KC_F4,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        KC_G, KC_LSFT, KC_A, KC_S, KC_D, KC_F, KC_PSLS, KC_4, KC_5, KC_6, KC_MINS, KC_F5,
        // ├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
        KC_F3, KC_LCTL, KC_Z, KC_X, KC_C, KC_V, XXXXXXX, XXXXXXX, KC_BSPC, KC_1, KC_2, KC_3, KC_DOT, KC_F6,
        // └────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
        KC_I, KC_LALT, KC_SPC, KC_ENT, KC_0, TG(_GAM)
        // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
        ),

    [_I3W] = LAYOUT(
        // ┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, LGUI(KC_Q), XXXXXXX, LGUI(KC_UP), XXXXXXX, XXXXXXX, XXXXXXX, LGUI(KC_7), LGUI(KC_8), LGUI(KC_9), XXXXXXX, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤─────────────────                          ├────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, KC_LSFT, LGUI(KC_LEFT), LGUI(KC_DOWN), LGUI(KC_RGHT), LGUI(KC_SPC), XXXXXXX, LGUI(KC_4), LGUI(KC_5), LGUI(KC_6), KC_RSFT, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, KC_LCTL, XXXXXXX, LGUI(KC_S), XXXXXXX, LGUI(KC_P), XXXXXXX, XXXXXXX, XXXXXXX, LGUI(KC_1), LGUI(KC_2), LGUI(KC_3), KC_RCTL, XXXXXXX,
        // └────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
        XXXXXXX, XXXXXXX, XXXXXXX, LGUI(KC_GRV), LGUI(KC_0), XXXXXXX
        // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
        ),

    [_LRGB] = LAYOUT(
        // ┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        _______, _______, _______, _______, _______, _______, _______, RGB_VAD, RGB_HUI, RGB_VAI, _______, _______,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
        _______, _______, _______, _______, _______, _______, _______, RGB_SAD, RGB_HUD, RGB_SAI, _______, _______,
        // ├────────┼────────┼────────┼────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
        _______, _______, _______, _______, _______, _______, _______, RGB_SPD, RGB_MOD, RGB_SPI, _______, _______, _______, _______,
        // └────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
        TG(_LRGB), _______, _______, RGB_TOG, _______, _______
        // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
        ),

    [_F12] = LAYOUT(
        // ┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_F1, KC_F2, KC_F3, KC_F4, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┤─────────────────                          ├────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, KC_LCTL, KC_LALT, KC_LGUI, KC_LSFT, XXXXXXX, XXXXXXX, KC_F5, KC_F6, KC_F7, KC_F8, XXXXXXX,
        // ├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┤────────┼────────┼────────┼────────┼────────┼
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_F9, KC_F10, KC_F11, KC_F12, XXXXXXX,
        // └────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX
        // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
        ),

};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
	// only with the hands down layer
	if (default_layer_state == 0x01 && process_adaptive_keys(keycode, record)) {
		return false;
	}
	if (process_linger_keys(keycode, record)) {
		return false;
	}
	switch (keycode) {
		case H_W:
				if (record->event.pressed) {
						tap_code16(KC_W);
						tap_code16(KC_H);
				}
				break;
		case H_G:
				if (record->event.pressed) {
						tap_code16(KC_G);
						tap_code16(KC_H);
				}
				break;
		case H_S:
				if (record->event.pressed) {
						tap_code16(KC_S);
						tap_code16(KC_H);
				}
				break;
		case H_P:
				if (record->event.pressed) {
						tap_code16(KC_P);
						tap_code16(KC_H);
				}
				break;
		case H_T:
				if (record->event.pressed) {
						tap_code16(KC_T);
						tap_code16(KC_H);
				}
				break;
		case H_C:
				if (record->event.pressed) {
						tap_code16(KC_C);
						tap_code16(KC_H);
				}
				break;
		case H_Sh:
				if (record->event.pressed) {
						tap_code16(KC_S);
						tap_code16(KC_H);
				}
				break;
		case H_Sch:
				if (record->event.pressed) {
						tap_code16(KC_S);
						tap_code16(KC_C);
						tap_code16(KC_H);
				}
				break;
	}
	return true;
}
