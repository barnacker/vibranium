#include "action_util.h"
#include "progmem.h"

#include "linger_keys_def.h"

static uint32_t linger_timer = 0;
static int pattern_count = sizeof(linger_patterns) / sizeof(linger_patterns[0]);

// This function is called from process_record_user
bool process_linger_keys(uint16_t keycode, keyrecord_t *record) {
	uint16_t linger_key;
	uint16_t output_sequence[3];
	keycode = keycode & 0xFF;

	for (int i = 0; i < pattern_count; i++) {
		linger_key = pgm_read_byte(&linger_patterns[i][0][0]);
		if (linger_key == keycode) {
			if (record->event.pressed) {
				linger_timer = timer_read32();
				for (int m = 0; m < 3; m++) {
					output_sequence[m] = pgm_read_byte(&linger_patterns[i][1][m]);
					if (output_sequence[m] == KC_NO) {
						break;
					}
					tap_code16(output_sequence[m]);
				};
				return true;
			} else {
				if (timer_elapsed32(linger_timer) >= LINGER_TERM) {
					for (int m = 0; m < 3; m++) {
						output_sequence[m] = pgm_read_byte(&linger_patterns[i][2][m]);
						if (output_sequence[m] == KC_NO) {
							break;
						}
						tap_code16(output_sequence[m]);
					};
					return true;
				}
			}
			return true;
		}
	}
	return false;
}
