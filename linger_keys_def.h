// I defined custom linger keycodes in the keymap.h
// because I still want regular keys like Q to be normal in my gaming layer.
const uint16_t PROGMEM linger_patterns[][3][3] = {
	// Custom linger code, what to type immediately, what to type after linger
  {{LINGER_Q},{KC_Q, KC_U, KC_NO}, {KC_BSPC, KC_NO, KC_NO}},
};
