HSV safe_hsv_value(HSV value) {
    HSV hsv = value;
    if (hsv.v > rgb_matrix_get_val()) {
        hsv.v = rgb_matrix_get_val();
    };
    return hsv;
}

void set_underglow(HSV value) {
    HSV hsv = safe_hsv_value(value);
    RGB rgb = hsv_to_rgb(hsv);
    for (uint8_t i = 0; i < RGB_MATRIX_LED_COUNT; i++) {
        if (HAS_FLAGS(g_led_config.flags[i], LED_FLAG_UNDERGLOW)) {
            rgb_matrix_set_color(i, rgb.r, rgb.g, rgb.b);
        }
    }
}

bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    HSV base  = {HSV_WHITE};
    HSV fnk   = {RGB_AZURE};
    HSV opt   = {HSV_WHITE};
    HSV fun   = {HSV_GREEN};
    HSV cap   = {HSV_GREEN};
    RGB green = hsv_to_rgb(safe_hsv_value(cap));
    RGB red   = hsv_to_rgb(safe_hsv_value(base));
    RGB blue  = hsv_to_rgb(safe_hsv_value(opt));

    int layer = get_highest_layer(layer_state);

    for (uint8_t row = 0; row < MATRIX_ROWS; ++row) {
        for (uint8_t col = 0; col < MATRIX_COLS; ++col) {
            uint8_t index = g_led_config.matrix_co[row][col];
            if (index >= led_min && index < led_max && index != NO_LED) {
                if (layer > _QWERTY) {
                    HSV hsv;
                    switch (layer) {
                        case _FNK:
                            hsv = opt;
                            break;
                        case _OPT:
                            hsv = fnk;
                            break;
                        case _GAM:
                            hsv = fun;
                            break;
                        default:
                            hsv = base;
                            break;
                    }
                    RGB rgb = hsv_to_rgb(safe_hsv_value(hsv));

                    if (keymap_key_to_keycode(layer, (keypos_t){col, row}) > KC_TRNS) {
                        rgb_matrix_set_color(index, rgb.r, rgb.g, rgb.b);
                    }
                }
                if (layer == _FNK) {
                    if ((keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_UP || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_DOWN || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_LEFT || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_RGHT)) {
                        rgb_matrix_set_color(index, green.r, green.g, green.b);
                    }
                    if ((keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_0 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_1 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_2 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_3 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_4 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_5 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_6 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_7 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_8 || keymap_key_to_keycode(layer, (keypos_t){col, row}) == KC_9)) {
                        rgb_matrix_set_color(index, green.r, green.g, green.b);
                    }
                }
                if (layer > _QWERTY && keymap_key_to_keycode(layer, (keypos_t){col, row}) == XXXXXXX) {
                    rgb_matrix_set_color(index, 0, 0, 0);
                }
                if (layer == _QWERTY || layer == _GAM) {
                    if (keymap_key_to_keycode(layer, (keypos_t){col, row}) == TG(_QWERTY) || keymap_key_to_keycode(layer, (keypos_t){col, row}) == TG(_GAM)) {
                        rgb_matrix_set_color(index, red.r, red.g, red.b);
                    }
                }
                if (layer == _QWERTY || layer == _BASE) {
                    if (capsOn) {
                        if (keymap_key_to_keycode(layer, (keypos_t){col, row}) == LSFT_T(KC_T) || keymap_key_to_keycode(layer, (keypos_t){col, row}) == RSFT_T(KC_A) || keymap_key_to_keycode(layer, (keypos_t){col, row}) == LSFT_T(KC_F) || keymap_key_to_keycode(layer, (keypos_t){col, row}) == RSFT_T(KC_J)) {
                            rgb_matrix_set_color(index, green.r, green.g, green.b);
                        }
                    }
                }
                if (layer == _OPT) {
                    if (keymap_key_to_keycode(layer, (keypos_t){col, row}) == EE_CLR) {
                        rgb_matrix_set_color(index, blue.r, blue.g, blue.b);
                    }
                    if (keymap_key_to_keycode(layer, (keypos_t){col, row}) == QK_BOOT) {
                        rgb_matrix_set_color(index, green.r, green.g, green.b);
                    }
                }
            }
        }
    }
    // if is on querty layer, set underglow to base color
    if (default_layer_state == 0x02) {
        set_underglow(fun);
    }

    return false;
}
