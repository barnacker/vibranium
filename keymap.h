#include QMK_KEYBOARD_H
#include "keycodes.h"
#include "quantum_keycodes.h"
#include "transactions.h"
#include <quantum.h>
#include "action.h"

enum custom_keycodes {
  H_W = SAFE_RANGE,
  H_G,
	H_S,
  H_P,
  H_T,
  H_C,
  H_Sh,
  H_Sch,
	LINGER_Q
};

#define _BASE 0
#define _QWERTY 1
#define _OPT 2
#define _FNK 3
#define _GAM 4
#define _I3W 5
#define _LRGB 6
#define _F12 7

bool capsOn     = false; // CapsWord is off by default
bool capsOnSync = false;
